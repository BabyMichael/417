package pages;

import org.openqa.selenium.By;

import util.DriverUtil;


public class MainAmazonPage
{
	public DriverUtil _driver;
	
	private final By yourAccountButton = new By.ById("nav-link-yourAccount");


	public MainAmazonPage(DriverUtil driver)
	{
		_driver = driver;
	}


	public SignInPage clickYourAccountButton()
	{
		_driver.click(yourAccountButton);
		return new SignInPage(_driver);
	}
}
