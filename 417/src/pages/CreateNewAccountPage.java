package pages;

import org.openqa.selenium.By;

import util.DriverUtil;


public class CreateNewAccountPage
{
	public DriverUtil _driver;

	public final By nameErrorMessage = new By.ByXPath("//div[@class = 'a-alert-content' and contains(text(), 'Enter your name')]");

	private final By createButton = new By.ById("continue");
	private final By nameField = new By.ById("ap_customer_name");
	private final By emailField = new By.ById("ap_email");
	private final By password = new By.ById("ap_password");
	private final By passwordAgain = new By.ById("ap_password_check");


	public CreateNewAccountPage(DriverUtil driver)
	{
		_driver = driver;
	}


	public boolean isNameErrorMessageVisible()
	{
		return _driver.clickIfExists(nameErrorMessage);
	}


	public void clickCreateExpectError()
	{
		_driver.click(createButton);
	}


	public void enterName(String name)
	{
		_driver.sendKeys(nameField, name);
	}


	public void enterEmail(String email)
	{
		_driver.sendKeys(emailField, email);
	}


	public void enterPassword(String password)
	{
		_driver.sendKeys(this.password, password);
	}


	public void enterPasswordAgain(String password2)
	{
		_driver.sendKeys(passwordAgain, password2);
	}
}
