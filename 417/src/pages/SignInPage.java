package pages;

import org.openqa.selenium.By;

import util.DriverUtil;


public class SignInPage
{
	public DriverUtil _driver;
	private final By createAccountButton = new By.ById("createAccountSubmit");


	public SignInPage(DriverUtil driver)
	{
		_driver = driver;
	}



	public CreateNewAccountPage clickCreateAccountButton()
	{
		_driver.click(createAccountButton);
		return new CreateNewAccountPage(_driver);
	}
}
