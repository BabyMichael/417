package tests;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import pages.CreateNewAccountPage;
import pages.MainAmazonPage;
import pages.SignInPage;
import util.DriverUtil;


public class Tests
{

	public DriverUtil driver;
	public MainAmazonPage mainPage;


	@Before
	public void setUp()
	{
		driver = new DriverUtil();
		mainPage = new MainAmazonPage(driver);
	}
	
	@After
	public void cleanUp()
	{
		driver.quitDriver();
	}


	@Test
	public void signUpNoName()
	{
		SignInPage signInPage = mainPage.clickYourAccountButton();

		CreateNewAccountPage createNewAccountPage = signInPage.clickCreateAccountButton();

		createNewAccountPage.enterEmail("name@gmail.com");
		createNewAccountPage.enterPassword("password");
		createNewAccountPage.enterPasswordAgain("password");
		createNewAccountPage.clickCreateExpectError();

		assertTrue("Error message appears", createNewAccountPage.isNameErrorMessageVisible());
	}

}
